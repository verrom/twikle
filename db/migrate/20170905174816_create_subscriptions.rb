class CreateSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :subscriptions do |t|
      t.string :email
      t.boolean :email_confirmed
      t.string :confirm_token
      t.boolean :news_receiver

      t.timestamps
    end
  end
end
