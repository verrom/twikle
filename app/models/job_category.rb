# == Schema Information
#
# Table name: job_categories
#
#  id                  :integer          not null, primary key
#  title               :string
#  priority            :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  slug                :string
#  link_text           :string
#  description         :text
#  description_visible :boolean          default(FALSE)
#  description_title   :string
#  seo_title           :string
#  seo_keywords        :string
#  seo_description     :string
#  search_phrase       :string
#  hh_specialization   :string
#  short_title         :string
#

class JobCategory < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :history]

  validates :title, :short_title, :priority, presence: true

  has_many :jobs, dependent: :nullify
  has_many :job_categories_subscriptions, dependent: :destroy
  has_many :subscriptions, through: :job_categories_subscriptions

  scope :sorted, -> { order(priority: :desc) }

  def any_confirmed_approved_jobs?(job_category)
    approved_jobs(job_category).exists?
  end

  def approved_jobs_count(job_category)
    approved_jobs(job_category).count
  end

  def should_generate_new_friendly_id?
    title_changed?
  end

  def next_job_category
    self.class.where('id > ?', id).first || JobCategory.first
  end

  def previous_job_category
    self.class.where('id < ?', id).last || JobCategory.last
  end

  private

  def approved_jobs(job_category)
    job_category.jobs.where(approved: true)
  end
end
