class CreateJobCategoriesSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_join_table :job_categories, :subscriptions do |t|
      t.index [:job_category_id, :subscription_id],
              name: 'index_job_categories_subscriptions'
    end
  end
end
