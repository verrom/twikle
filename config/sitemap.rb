# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'http://twikle.ru'
SitemapGenerator::Sitemap.ping_search_engines
SitemapGenerator::Sitemap.sitemaps_path = 'shared/'

SitemapGenerator::Sitemap.create do
  Article.find_each do |article|
    add article_path(article), lastmod: article.updated_at
  end

  JobCategory.find_each do |category|
    add job_category_path(category), lastmod: category.updated_at
  end

  Job.find_each do |job|
    add job_path(job), lastmod: job.updated_at if job.approved?
  end
end
