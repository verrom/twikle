# == Schema Information
#
# Table name: articles
#
#  id              :integer          not null, primary key
#  title           :string
#  content         :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  slug            :string
#  seo_title       :string
#  seo_keywords    :string
#  seo_description :string
#  approved        :boolean          default(FALSE)
#  promo_text      :string
#

require 'rails_helper'

RSpec.describe Article, type: :model do
  context 'validations' do
    it { should validate_presence_of :title }
    it { should validate_presence_of :text  }
  end
end
