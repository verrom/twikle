class SendNewArticlesService
  def send_new_articles_every_week
    select_articles_subscribers && select_last_week_approved_articles
    return if @last_week_approved_articles.blank?

    @articles_subscribers.each do |subscriber|
      SubscriptionMailer
        .send_new_articles_every_week(
          subscriber,
          @last_week_approved_articles
        ).deliver_later
    end
  end

  def select_last_week_approved_articles
    @last_week_approved_articles = []
    Article.where('updated_at > ?', 7.days.ago)
           .where(approved: true).each do |article|
      if article.versions.last.created_at > 7.days.ago
        @last_week_approved_articles << article
      end
    end
  end

  def select_articles_subscribers
    @articles_subscribers = Subscription.where(email_confirmed: true,
                                               news_receiver: true)
  end
end
