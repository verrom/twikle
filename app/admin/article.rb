ActiveAdmin.register Article do
  permit_params :title, :content, :seo_title, :seo_keywords, :seo_description,
                :approved, :promo_text

  index do
    selectable_column
    column :title
    column :approved
    actions
  end

  filter :title
  filter :content
  filter :created_at

  form do |f|
    f.semantic_errors(*f.object.errors.keys)
    f.inputs do
      f.input :title, label: 'Заголовок'
    end

    f.inputs 'Текст статьи' do
      f.input :content, as: :ckeditor
    end

    f.inputs 'Промо текст' do
      f.input :promo_text
    end

    f.inputs 'SEO поля' do
      f.input :seo_title,       label: 'SEO title'
      f.input :seo_keywords,    label: 'SEO keywords'
      f.input :seo_description, label: 'SEO description'
    end

    f.inputs 'Модерация' do
      f.input :approved, as: :boolean, label: 'Модерация пройдена'
    end

    f.actions
  end

  controller do
    def find_resource
      scoped_collection.find_by!(slug: params[:id])
    end
  end
end
