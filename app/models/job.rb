# == Schema Information
#
# Table name: jobs
#
#  id              :integer          not null, primary key
#  title           :string
#  office_location :string
#  description     :text
#  how_to_apply    :text
#  company_title   :string
#  company_url     :string
#  email           :string
#  job_category_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  approved        :boolean          default(FALSE)
#  email_confirmed :boolean          default(FALSE)
#  confirm_token   :string
#  slug            :string
#  unique          :boolean          default(FALSE)
#  salary_min      :integer
#  salary_max      :integer
#  currency        :string
#  promo_text      :string
#  external_id     :integer
#  provider        :string           default("user")
#  employment      :string
#  expired         :boolean          default(FALSE)
#

class Job < ApplicationRecord
  extend FriendlyId
  include PgSearch
  include Filterable

  has_paper_trail on: :update
  has_paper_trail only: [:approved]

  pg_search_scope :search_everywhere,
                  against: [:title, :description, :company_title],
                  using: {
                    tsearch: {
                      prefix: true
                    }
                    # :trigram => {
                    #   :threshold => 0.1
                    # }
                  }
  scope :published, -> { where(approved: true) }
  scope :not_expired, -> { where(expired: false) }
  scope :sorted, -> { order(created_at: :desc) }
  scope :sorted_with_reorder, -> { reorder(created_at: :desc) }
  scope :join_tags, -> { includes(:tag_taggings) }
  scope :salary_min, lambda { |salary_min|
    where('(salary_min >= ?) or (salary_max >= ?) or
          (salary_min IS NULL and salary_max IS NULL)', salary_min, salary_min)
  }
  scope :salary_max, lambda { |salary_max|
    where('salary_min <= ? or salary_min IS NULL', salary_max)
      .where('salary_max <= ? or salary_max IS NULL', salary_max)
  }
  scope :by_category, lambda { |*category_id|
    joins(:job_category).where(job_category_id: category_id)
  }
  scope :by_tags, lambda { |*tag_name|
    joins(:tags).where(tags: { name: tag_name }).distinct
  }
  scope :by_currency, lambda { |currency|
    where('currency = ? or (salary_min IS NULL and salary_max IS NULL)', currency)
  }
  scope :by_employment, ->(employment) { where(employment: employment) }

  friendly_id :slug_candidates, use: [:slugged, :history]

  belongs_to :job_category

  acts_as_taggable_on :tags

  def slug_candidates
    [
      [Job.maximum(:id) + 1, :title, 'v', :company_title]
    ]
  end

  def should_generate_new_friendly_id?
    title_changed? || company_title_changed?
  end

  before_create :confirmation_token

  validates :title, :description, :how_to_apply,
            :company_title, :email, :job_category_id, presence: true
  validates :salary_min, :salary_max, numericality: { only_integer: true },
                                      inclusion: { in: 1..50_000_000 },
                                      allow_blank: true
  validates_uniqueness_of :external_id, scope: :provider,
                                        allow_blank: true,
                                        allow_nil: true

  validates_uniqueness_of :title, uniqueness: { scope: :company_title },
    case_sensitive: false,
    conditions: -> { where(expired: false) }

  def email_activate
    self.email_confirmed = true
    self.confirm_token = nil
    save!(validate: false)
  end

  def short_description(max_length = nil)
    max_length ||= 75
    return unless description.present?

    Truncato.truncate ActionController::Base.helpers.strip_tags(description), max_length: max_length, count_tags: false
  end

  def next_job
    Job.published.not_expired.where('id > ?', id)
       .where(job_category: job_category).first ||
      Job.published.not_expired.where(job_category: job_category).first
  end

  def previous_job
    Job.published.not_expired.where('id < ?', id)
       .where(job_category: job_category).last ||
      Job.published.not_expired.where(job_category: job_category).last
  end

  def salary_for_views
    self.currency = 'руб.' if self.currency == 'RUR'
    return 'з/п не указана' unless salary_min || salary_max
    return "#{salary_min} #{currency}" if salary_min == salary_max
    return "от #{salary_min} #{currency}" if salary_min? && salary_max.blank?
    return "до #{salary_max} #{currency}" if salary_min.blank? && salary_max?
    return "от #{salary_min} до #{salary_max} #{currency}" if salary_min? && salary_max?
  end

  private

  def confirmation_token
    return unless confirm_token.blank?
    self.confirm_token = SecureRandom.urlsafe_base64.to_s
  end
end
