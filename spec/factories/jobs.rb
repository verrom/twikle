# == Schema Information
#
# Table name: jobs
#
#  id              :integer          not null, primary key
#  title           :string
#  office_location :string
#  description     :text
#  how_to_apply    :text
#  company_title   :string
#  company_url     :string
#  email           :string
#  job_category_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  approved        :boolean          default(FALSE)
#  email_confirmed :boolean          default(FALSE)
#  confirm_token   :string
#  slug            :string
#  unique          :boolean          default(FALSE)
#  salary_min      :integer
#  salary_max      :integer
#  currency        :string
#  promo_text      :string
#  external_id     :integer
#  provider        :string           default("user")
#  employment      :string
#  expired         :boolean          default(FALSE)
#

FactoryGirl.define do
  factory :job do
    title 'Programmer'
    description 'Some text here'
    how_to_apply 'Write us'
    company_title 'Good Company'
    email 'Mail@good-company.com'
  end
end
