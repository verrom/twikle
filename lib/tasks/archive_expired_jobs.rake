desc 'Archive expired jobs' # TODO: Update crontab https://github.com/javan/whenever
task archive_expired_jobs: :environment do
  Job.where.not(provider: 'moikrug').find_each(batch_size: 100) do |job|
    job.update_column('expired', true) if (Time.now - job.created_at) / 86_400 > 30
  end
end
