# == Schema Information
#
# Table name: job_categories
#
#  id                  :integer          not null, primary key
#  title               :string
#  priority            :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  slug                :string
#  link_text           :string
#  description         :text
#  description_visible :boolean          default(FALSE)
#  description_title   :string
#  seo_title           :string
#  seo_keywords        :string
#  seo_description     :string
#  search_phrase       :string
#  hh_specialization   :string
#  short_title         :string
#

FactoryGirl.define do
  factory :job_category do
    title 'Programming'
    priority 100
  end
end
