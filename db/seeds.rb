# This file should contain all the record creation needed to seed the database
# with its default values.
# The data can then be loaded with the rails db:seed command (or created
# alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the
# Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password',
                  password_confirmation: 'password')
JobCategory.create!(title: 'Программирование', priority: 100)

100.times do
  Job.create!(
    title: 'Rails programmer',
    description: 'But I must explain to you how all this mistaken idea of ' \
    'denouncing pleasure and praising pain was born and I will give you a ' \
    'complete account of the system, and expound the actual teachings of the ' \
    'great explorer of the truth, the master-builder of human happiness. No ' \
    'one rejects, dislikes, or avoids pleasure itself, because it is ' \
    'pleasure, but because those who do not know how to pursue pleasure ' \
    'rationally encounter consequences that are extremely painful. Nor again ' \
    'is there anyone who loves or pursues or desires to obtain pain of ' \
    'itself, because it is pain, but because occasionally circumstances ' \
    'occur in which toil and pain can procure him some great pleasure.',
    how_to_apply: 'write us on apply@somemail.com',
    company_title: 'Some Company',
    email: 'apply@somemail.com',
    job_category_id: JobCategory.last.id,
    email_confirmed: true,
    approved: true
  )
end
