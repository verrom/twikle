class AddNewColumnToJobCategoriesSubscription < ActiveRecord::Migration[5.1]
  def change
    add_column :job_categories_subscriptions, :id, :primary_key
  end
end
