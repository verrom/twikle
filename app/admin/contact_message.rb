ActiveAdmin.register ContactMessage do
  permit_params :name, :message, :email

  index do
    selectable_column
    column :name
    column :email
    column :created_at
    actions
  end

  filter :name
  filter :email
  filter :message
  filter :created_at

  form do |f|
    f.inputs 'Message Details' do
      f.input :name
      f.input :email
      f.input :message
    end
    f.actions
  end
end
