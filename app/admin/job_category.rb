ActiveAdmin.register JobCategory do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :title, :short_title, :priority, :link_text, :description,
                :description_title, :description_visible,
                :seo_title, :seo_keywords, :seo_description,
                :hh_specialization, :search_phrase

  index do
    column :title
    column :priority
    actions
  end

  form do |f|
    f.semantic_errors(*f.object.errors.keys)
    f.inputs 'Детали категории вакансий' do
      f.input :title
      f.input :short_title, label: 'Короткое название категории (для фильтров)'
      f.input :priority
      f.input :link_text
      f.trix_editor :description
      f.input :description_title
      f.input :description_visible, as: :boolean
      f.input :hh_specialization
      f.input :search_phrase
    end

    f.inputs 'SEO поля' do
      f.input :seo_title,       label: 'SEO title'
      f.input :seo_keywords,    label: 'SEO keywords'
      f.input :seo_description, label: 'SEO description'
    end

    f.actions
  end

  # find record with slug(friendly_id)
  controller do
    def find_resource
      scoped_collection.find_by!(slug: params[:id])
    end
  end
end
