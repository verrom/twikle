$(document).on 'ready turbolinks:load', ->
  return unless $('.js-example-basic-multiple').length > 0

  max = $('.js-example-basic-multiple').data('max')
  $('.js-example-basic-multiple').select2
    maximumSelectionLength: max
    theme: 'bootstrap'
