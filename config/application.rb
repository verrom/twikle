require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Twikle
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.i18n.default_locale = :ru
    config.exceptions_app = self.routes
    config.autoload_paths += %w(#{config.root}/app/models/ckeditor)
    config.assets.precompile += %w( ckeditor/* )

    config.active_job.queue_adapter = :sidekiq

    config.assets.paths << Rails.root.join('vendor', 'assets', 'fonts')
    config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/

    config.middleware.use Rack::Deflater

    # allows to use subfolded models without namespaces
    config.autoload_paths << Rails.root.join('lib')
    config.autoload_paths << Rails.root.join('app', 'services', 'mailers')
    config.autoload_paths << Rails.root.join('app', 'services', 'subscriptions')
    config.autoload_paths << Rails.root.join('app', 'services', 'parsers')

    # fix deploy error: NameError: uninitialized constant Rack::Rewrite
    # config.middleware.insert_before(::Rack::Runtime, ::Rack::Rewrite) do
    #   r301 %r{^/(.*)/$}, '/$1'
    # end
  end
end
