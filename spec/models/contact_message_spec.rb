# == Schema Information
#
# Table name: contact_messages
#
#  id         :integer          not null, primary key
#  name       :string
#  message    :text
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe ContactMessage, type: :model do
  context 'validations' do
    it { should validate_presence_of :name    }
    it { should validate_presence_of :message }
    it { should validate_presence_of :email   }
  end
end
