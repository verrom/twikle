class GetDataFromHhApiService
  include HTTParty
  include ActionView::Helpers::UrlHelper
  base_uri 'https://api.hh.ru'

  def save_data_from_hh_api
    @job_categories = JobCategory.where.not(search_phrase: "")
    @job_categories.each do |cat|
      @jobs_urls = []
      get_jobs_data(cat)
      get_jobs_urls(@job_data)
      check_for_pages_count(@job_data, cat)
      save_new_jobs(@job_data, cat, @jobs_urls)
    end
  end

  private

  def get_jobs_urls(job_data)
    job_data['items'].map { |item| @jobs_urls << item['url'] if item['url'] }
  end

  def save_new_jobs(job_data, cat, jobs_urls)
    jobs_urls.each do |job_url|
      response = self.class.get(job_url, headers: {'User-Agent': 'info@twikle.ru'})
      job_data = JSON.parse(response.body)
      job = Job.new
      job.external_id = job_data['id']
      job.title = job_data['name']
      job.description = job_data['description']
      job.how_to_apply = "Откликнуться на данную вакансию можно на сайте #{link_to 'HeadHunter', job_data['alternate_url']}"
      job.company_title = job_data['employer']['name']
      job.email = 'hh@'
      job.job_category_id = cat.id
      job.provider = 'headhunter'
      job.approved = true
      job.unique = true
      job.office_location = job_data['area']['name'] if job_data['area']
      job.salary_min = job_data['salary']['from'] if job_data['salary']
      job.salary_max = job_data['salary']['to'] if job_data['salary']
      job.currency = job_data['salary']['currency'] if job_data['salary']
      job.employment = job_data['employment']['id']

      if job.save
        save_tags_for_job(job_data, job)
      end
    end
  end

  def get_jobs_data(cat, page_num=0)
    page_numb = "page=#{page_num}"
    specialization = "specialization=#{cat.hh_specialization}&" if !cat.hh_specialization.blank?
    search_phrase = "text=#{cat.search_phrase}&" if !cat.search_phrase.blank?
    relative_url = "/vacancies?period=1&#{specialization}#{search_phrase}schedule=remote&area=113&search_field=name&#{page_numb}&premium=true"
    encoded_url = URI.encode(relative_url)
    response = self.class.get(encoded_url, headers: {'User-Agent': 'api-test-agent'})
    @job_data = JSON.parse(response.body)
  end

  def check_for_pages_count(job_data, cat)
    if job_data['pages'] > 1
      pages_numbers = []
      (1...job_data['pages']).map { |pages_num| pages_numbers << pages_num }
      pages_numbers.each do |page_num|
        get_jobs_data(cat, page_num)
        get_jobs_urls(@job_data)
      end
    end
  end

  def save_tags_for_job(job_data, job)
    exist_tags = Tag.pluck(:name).uniq # update after saving each job
    exist_tags_downcased = exist_tags.map(&:downcase).uniq
    job_tags = []
    unless job_data['key_skills'].empty?
      job_data['key_skills'].map { |skill| job_tags << skill['name'] }
      job_tags_downcased = job_tags.map(&:downcase)
      intersections = []
      intersections = job_tags_downcased & exist_tags_downcased
      if intersections.any?
        intersections.each do |intersec|
          job.tags << ActsAsTaggableOn::Tag.where('lower(name) = ?', intersec).first
          job_tags.delete_if { |t| t.casecmp(intersec) == 0 }
        end
      end
      job_tags.map { |tag| job.tags.create(name: tag) }
    end
  end
end
