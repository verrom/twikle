# == Schema Information
#
# Table name: subscriptions
#
#  id               :integer          not null, primary key
#  email            :string
#  email_confirmed  :boolean
#  confirm_token    :string
#  news_receiver    :boolean
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  frequency        :integer
#  salary           :integer
#  unsubscribe_hash :string
#

class Subscription < ApplicationRecord
  validates :email, presence: true, uniqueness: { case_sensitive: false }
  # format: /\A[^@\s]+@[^@\s]+\z/
  validates :salary, numericality: { only_integer: true },
                     inclusion: { in: 1..50_000_000 }, allow_blank: true

  has_many :job_categories_subscriptions, dependent: :destroy
  has_many :job_categories, through: :job_categories_subscriptions

  before_save :downcase_email
  before_create :confirmation_token, :add_unsubscribe_hash

  acts_as_taggable_on :programmings

  def email_activate
    self.email_confirmed = true
    self.confirm_token = nil
    save!(validate: false)
  end

  private

  def add_unsubscribe_hash
    self.unsubscribe_hash = SecureRandom.urlsafe_base64.to_s
  end

  def confirmation_token
    return unless confirm_token.blank?
    self.confirm_token = SecureRandom.urlsafe_base64.to_s
  end

  def downcase_email
    self.email = email.downcase if email
  end
end
