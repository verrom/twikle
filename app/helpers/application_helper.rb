module ApplicationHelper
  def url_with_protocol(url)
    /^http/i.match(url) ? url : "http://#{url}"
  end

  def add_nofollow(html)
    html.scan(/(\<a href=["'].*?["']\>.*?\<\/a\>)/).flatten.each do |link|
      if link.match(/\<a href=["'](http:\/\/|www){0,1}((localhost:3000|mysite.com)(\/.*?){0,1}|\/.*?)["']\>(.*?)\<\/a\>/)
      else
        link.match(/(\<a href=["'](.*?)["']\>(.*?)\<\/a\>)/)
        html.gsub!(link, "<a href='#{$2}' rel='nofollow' target='_new' >#{$3}</a>")
      end
    end
    html
  end
end
