class AddShortTitleToJobCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :job_categories, :short_title, :string
  end
end
