# == Schema Information
#
# Table name: text_blocks
#
#  id         :integer          not null, primary key
#  title      :string
#  content    :text
#  place      :string
#  visible    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TextBlock < ApplicationRecord
  # scope :proper_visible_text_selected,
  #       -> { where(place: 'home', visible: true) }
  def self.homepage_text
    find_by(place: 'home', visible: true)
  end
end
