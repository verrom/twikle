ActiveAdmin.register Tag do
  permit_params :name, :slug, taggings_attributes: %i[id context _destroy]

  filter :name

  index do
    selectable_column
    id_column
    column :name
    actions
  end

  form do |f|
    f.semantic_errors(*f.object.errors.keys)
    f.inputs 'Название' do
      f.input :name
    end

    f.inputs 'Контекст' do
      f.has_many(
        :taggings,
        new_record: 'Добавить контекст',
        heading: false,
        allow_destroy: true
      ) do |t|
        t.input :context
      end
    end

    f.actions
  end
end
