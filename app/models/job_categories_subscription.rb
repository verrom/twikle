# == Schema Information
#
# Table name: job_categories_subscriptions
#
#  job_category_id :integer          not null
#  subscription_id :integer          not null
#  id              :integer          not null, primary key
#

class JobCategoriesSubscription < ApplicationRecord
  belongs_to :subscription
  belongs_to :job_category
end
