# == Schema Information
#
# Table name: jobs
#
#  id              :integer          not null, primary key
#  title           :string
#  office_location :string
#  description     :text
#  how_to_apply    :text
#  company_title   :string
#  company_url     :string
#  email           :string
#  job_category_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  approved        :boolean          default(FALSE)
#  email_confirmed :boolean          default(FALSE)
#  confirm_token   :string
#  slug            :string
#  unique          :boolean          default(FALSE)
#  salary_min      :integer
#  salary_max      :integer
#  currency        :string
#  promo_text      :string
#  external_id     :integer
#  provider        :string           default("user")
#  employment      :string
#  expired         :boolean          default(FALSE)
#

require 'rails_helper'

RSpec.describe Job, type: :model do
  context 'db columns' do
    it do
      should have_db_column(:email_confirmed).of_type(:boolean)
                                             .with_options(default: false)
    end
    it do
      should have_db_column(:approved).of_type(:boolean)
                                      .with_options(default: false)
    end
    it do
      should have_db_column(:unique).of_type(:boolean)
                                    .with_options(default: false)
    end
  end

  context 'validations' do
    it { should validate_presence_of :title         }
    it { should validate_presence_of :description   }
    it { should validate_presence_of :how_to_apply  }
    it { should validate_presence_of :company_title }
    it { should validate_presence_of :email         }
  end

  context 'associations' do
    it { should belong_to(:job_category) }
  end

  describe 'token generation' do
    let(:job_category) { create :job_category }
    let(:job) { create :job, job_category_id: job_category.id }

    it 'has confirmation_token after save' do
      expect(job.confirm_token).not_to be_blank
    end
  end

  describe 'slug generation' do
    let(:job_category) { create :job_category }
    let(:job) { create :job, job_category_id: job_category.id }

    it 'has proper slug after save' do
      expect(job.slug).to eq 'programmer-v-good-company'
    end
  end
end
