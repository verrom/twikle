class AddLinkTextToJobCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :job_categories, :link_text, :string
  end
end
