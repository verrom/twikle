ActiveAdmin.register TextBlock do
  permit_params :title, :content, :place, :visible

  index do
    selectable_column
    id_column
    column :title
    column :place
    column :visible
    column :created_at
    actions
  end

  filter :title
  filter :place
  filter :content

  form do |f|
    f.semantic_errors(*f.object.errors.keys)
    f.inputs 'Детали текстового блока' do
      f.input :title
      f.input :place
      f.trix_editor :content
      f.input :visible, as: :boolean
    end
    f.actions
  end
end
