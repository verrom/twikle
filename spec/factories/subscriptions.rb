# == Schema Information
#
# Table name: subscriptions
#
#  id               :integer          not null, primary key
#  email            :string
#  email_confirmed  :boolean
#  confirm_token    :string
#  news_receiver    :boolean
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  frequency        :integer
#  salary           :integer
#  unsubscribe_hash :string
#

FactoryGirl.define do
  factory :subscription do
    email "MyString"
    email_confirmed false
    confirm_token "MyString"
    news_receiver false
  end
end
