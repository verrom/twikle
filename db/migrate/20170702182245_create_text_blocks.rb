class CreateTextBlocks < ActiveRecord::Migration[5.1]
  def change
    create_table :text_blocks do |t|
      t.string :title
      t.text :content
      t.string :place
      t.boolean :visible

      t.timestamps
    end
  end
end
