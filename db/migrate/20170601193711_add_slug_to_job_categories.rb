class AddSlugToJobCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :job_categories, :slug, :string
    add_index :job_categories, :slug
  end
end
