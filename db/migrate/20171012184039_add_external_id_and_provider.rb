class AddExternalIdAndProvider < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :external_id, :integer
    add_column :jobs, :provider, :string
  end
end
