class JobCategoriesController < ApplicationController
  before_action :force_trailing_slash

  def show
    @job_category = JobCategory.friendly.find(params[:id])
    if request.path != job_category_path(@job_category)
      redirect_to @job_category, status: :moved_permanently
    end
    @approved_jobs = @job_category.jobs.published.not_expired.sorted.join_tags
                                  .filter(params.slice(:salary_min, :by_tags,
                                                       :by_currency,
                                                       :by_employment))
                                  .page(params[:page]).per(15)
    @next_job_category = @job_category.next_job_category
    @previous_job_category = @job_category.previous_job_category
    @job_categories = JobCategory.order('priority DESC')

    seo_tags(@job_category)
  end

  private

  def seo_tags(job_category)
    set_meta_tags title: "#{t('.remote_work')} #{job_category.link_text}"
    set_meta_tags description: job_category.seo_description
    set_meta_tags keywords: job_category.seo_keywords
    set_meta_tags canonical: job_category_url(job_category)
  end
end
