# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every 4.hours do # 1.minute 1.day 1.week 1.month 1.year is also supported
  rake 'archive_expired_jobs'
end

every 1.day, at: '5:00 am' do
  rake '-s sitemap:refresh'
end

every 1.day, at: '9:00 am' do
  runner 'SendNewJobsService.new.send_daily_jobs'
end

every :monday, at: '10:00 am' do
  runner 'SendNewJobsService.new.send_weekly_jobs'
end

every :monday, at: '11:00 am' do
  runner 'SendNewArticlesService.new.send_new_articles_every_week'
end

every 3.hours do
  rake 'remove_unconfirmed_subscriptions'
end

every 3.hours do
  runner 'GetDataFromHhApiService.new.save_data_from_hh_api &
   AddTagsToJobsService.new.add_tags_to_jobs'
end

every 3.hours do
  runner 'GetDataFromMoikrugRssService.new.fetch_new_jobs &
    GetDataFromMoikrugRssService.new.archive_expired_jobs'
end
