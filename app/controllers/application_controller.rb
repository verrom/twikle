class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # if Rails.env.production?
  #   http_basic_authenticate_with name: 'terminator', password: 'VrYsTrNg05932'
  # end
  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def all_published_jobs
    @jobs = Job.published.not_expired.join_tags
               .filter(params.slice(:salary_min, :by_category,
                                    :by_currency, :by_tags, :by_employment))
               .sorted.page(params[:page]).per(15)
  end

  protected

  def force_trailing_slash
    redirect_to request.original_url.chomp!('/') if request.original_url.match(/\/$/)
  end
end
