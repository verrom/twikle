namespace :send_mails do # remove later
  desc 'Send daily mails'
  task send_new_jobs_every_day: :environment do
    filter_subscribers(1) && select_today_approved_jobs
    next if @today_approved_jobs.blank?

    @filtered_subscribers.each do |subscriber|
      jobs_to_send = []
      @today_approved_jobs.each do |job|
        if subscriber.job_categories
                     .include?(job.job_category) && salary_is_proper?(subscriber, job)
          jobs_to_send << job
        end
      end
      next unless jobs_to_send.any?
      SubscriptionMailer.send_new_jobs_every_day(subscriber, jobs_to_send)
                        .deliver
    end
  end

  desc 'Send weekly mails'
  task send_new_jobs_every_week: :environment do
    filter_subscribers(7) && select_last_week_approved_jobs
    return if @last_week_approved_jobs.blank?

    @filtered_subscribers.each do |subscriber|
      jobs_to_send = []
      @last_week_approved_jobs.each do |job|
        jobs_to_send << job if subscriber.job_categories
                                         .include? job.job_category
      end
      next unless jobs_to_send.any?
      SubscriptionMailer.send_new_jobs_every_week(subscriber, jobs_to_send)
                        .deliver
    end
  end

  desc 'Send weekly articles'
  task send_new_articles_every_week: :environment do
    select_articles_subscribers && select_last_week_approved_articles
    next if @last_week_approved_articles.blank?

    @articles_subscribers.each do |subscriber|
      SubscriptionMailer
        .send_new_articles_every_week(
          subscriber,
          @last_week_approved_articles
        ).deliver_later
    end
  end

  def select_today_approved_jobs
    @today_approved_jobs = []
    Job.where('updated_at > ?', 24.hours.ago)
       .where(approved: true).each do |job|
      @today_approved_jobs << job if job.versions.last.created_at > 24.hours.ago
    end
  end

  def select_last_week_approved_jobs
    @last_week_approved_jobs = []
    Job.where('updated_at > ?', 7.days.ago)
       .where(approved: true).each do |job|
      @last_week_approved_jobs << job if job.versions
                                            .last.created_at > 7.days.ago
    end
  end

  def select_last_week_approved_articles
    @last_week_approved_articles = []
    Article.where('updated_at > ?', 7.days.ago)
           .where(approved: true).each do |article|
      if article.versions.last.created_at > 7.days.ago
        @last_week_approved_articles << article
      end
    end
  end

  def filter_subscribers(frequency)
    @filtered_subscribers = Subscription.where(email_confirmed: true,
                                               frequency: frequency)
  end

  def select_articles_subscribers
    @articles_subscribers = Subscription.where(email_confirmed: true,
                                               news_receiver: true)
  end

  def salary_is_proper?(subscriber, job)
    convert_usd_to_rub(job)
    return true unless subscriber.salary && @salary_in_rub_max
    return true if @salary_in_rub_max >= subscriber.salary
  end

  def convert_usd_to_rub(job)
    if job.currency == 'USD'
      @salary_in_rub_min = job.salary_min * 60 if job.salary_min?
      @salary_in_rub_max = job.salary_max * 60 if job.salary_max?
    else
      @salary_in_rub_min = job.try(:salary_min)
      @salary_in_rub_max = job.try(:salary_max)
    end
  end
end
