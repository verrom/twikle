# == Schema Information
#
# Table name: articles
#
#  id              :integer          not null, primary key
#  title           :string
#  content         :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  slug            :string
#  seo_title       :string
#  seo_keywords    :string
#  seo_description :string
#  approved        :boolean          default(FALSE)
#  promo_text      :string
#

class Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged

  has_paper_trail on: :update
  has_paper_trail only: [:approved]

  validates :title, :content, presence: true

  scope :published, -> { where(approved: true) }

  def should_generate_new_friendly_id?
    title_changed?
  end

  def next_article
    Article.published.where('id > ?', id).first || Article.published.first
  end

  def previous_article
    Article.published.where('id < ?', id).last || Article.published.last
  end
end
