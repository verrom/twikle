# == Schema Information
#
# Table name: tags
#
#  id             :integer          not null, primary key
#  name           :string
#  taggings_count :integer          default(0)
#  slug           :string
#

class Tag < ApplicationRecord
  has_many :taggings, class_name: '::ActsAsTaggableOn::Tagging'
  accepts_nested_attributes_for :taggings, allow_destroy: true
end
