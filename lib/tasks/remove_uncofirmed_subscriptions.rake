desc 'Remove unconfirmed subscriptions'
task remove_unconfirmed_subscriptions: :environment do
  subscriptions = Subscription.where(email_confirmed: nil,
                                     created_at: 3.hours.ago..Time.now)
  subscriptions.destroy_all
end
