class AddEmailConfirmColumnToJobs < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :email_confirmed, :boolean, default: false
    add_column :jobs, :confirm_token, :string
  end
end
