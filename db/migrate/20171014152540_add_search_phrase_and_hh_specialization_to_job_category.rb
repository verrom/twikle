class AddSearchPhraseAndHhSpecializationToJobCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :job_categories, :search_phrase, :string
    add_column :job_categories, :hh_specialization, :string
  end
end
