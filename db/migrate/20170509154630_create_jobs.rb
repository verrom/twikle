class CreateJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :category
      t.string :office_location
      t.text :descrition
      t.text :how_to_apply
      t.string :company_title
      t.string :company_url
      t.string :email
      t.belongs_to :job_category, index: true

      t.timestamps
    end
  end
end
