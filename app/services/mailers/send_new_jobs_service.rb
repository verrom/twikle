class SendNewJobsService
  def send_daily_jobs
    filter_subscribers(1) && select_today_approved_jobs
    return if @today_approved_jobs.blank?

    @filtered_subscribers.each do |subscriber|
      jobs_to_send = []
      @today_approved_jobs.each do |job|
        jobs_to_send << job if all_verifications_passed(job, subscriber)
      end
      next unless jobs_to_send.any?
      SubscriptionMailer.send_new_jobs_every_day(subscriber, jobs_to_send)
                        .deliver_later
    end
  end

  def send_weekly_jobs
    filter_subscribers(7) && select_last_week_approved_jobs
    return if @last_week_approved_jobs.blank?

    @filtered_subscribers.each do |subscriber|
      jobs_to_send = []
      @last_week_approved_jobs.each do |job|
        jobs_to_send << job if all_verifications_passed(job, subscriber)
      end
      next unless jobs_to_send.any?
      SubscriptionMailer.send_new_jobs_every_week(subscriber, jobs_to_send)
                        .deliver_later
    end
  end

  private

  def select_today_approved_jobs
    @today_approved_jobs = []
    Job.where('updated_at > ?', 24.hours.ago)
       .where(approved: true).each do |job|
      @today_approved_jobs << job if job.versions.last.created_at > 24.hours.ago
    end
  end

  def select_last_week_approved_jobs
    @last_week_approved_jobs = []
    Job.where('updated_at > ?', 7.days.ago)
       .where(approved: true).each do |job|
      @last_week_approved_jobs << job if job.versions
                                            .last.created_at > 7.days.ago
    end
  end

  def filter_subscribers(frequency)
    @filtered_subscribers = Subscription.where(email_confirmed: true,
                                               frequency: frequency)
  end

  def all_verifications_passed(job, subscriber)
    return false unless subscriber.job_categories.include?(job.job_category)
    return false unless salary_is_proper?(subscriber, job)
    return false if rejected_by_programming_tags?(subscriber, job)
    true
  end

  def salary_is_proper?(subscriber, job)
    convert_usd_to_rub(job)
    return true unless subscriber.salary && @salary_in_rub_max
    return true if @salary_in_rub_max >= subscriber.salary
  end

  def convert_usd_to_rub(job)
    if job.currency == 'USD'
      @salary_in_rub_min = job.salary_min * 57 if job.salary_min?
      @salary_in_rub_max = job.salary_max * 57 if job.salary_max?
    elsif job.currency == 'EUR'
      @salary_in_rub_min = job.salary_min * 67 if job.salary_min?
      @salary_in_rub_max = job.salary_max * 67 if job.salary_max?
    else
      @salary_in_rub_min = job.try(:salary_min)
      @salary_in_rub_max = job.try(:salary_max)
    end
  end

  def rejected_by_programming_tags?(subscriber, job)
    return true unless subscribed_to_programming_jobs?(subscriber)
    return true unless job.tags.any? && subscriber.programmings.any?
    any_intersections(subscriber, job) ? false : true
  end

  def subscribed_to_programming_jobs?(subscriber)
    subscriber.job_categories.map { |cat| cat.title == 'Программирование' }.any?
  end

  def any_intersections(subscriber, job)
    !(job.tags & subscriber.programmings).empty?
  end
end
