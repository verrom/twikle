class AddUnsubscribeHashToSubscriptions < ActiveRecord::Migration[5.1]
  def change
    add_column :subscriptions, :unsubscribe_hash, :string
  end
end
