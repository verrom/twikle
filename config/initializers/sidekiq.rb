case "#{Rails.env}"
when 'production'
  db_num = 0
when 'develop'
  db_num = 1
end

Sidekiq.configure_server do |config|
  config.redis = { url: "redis://localhost:6379/#{db_num}" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://localhost:6379/#{db_num}" }
end
