class ArticlesController < ApplicationController
  before_action :force_trailing_slash

  def show
    @article = Article.published.friendly.find(params[:id])
    @next_article = @article.next_article
    @previous_article = @article.previous_article
    show_page_seo_tags(@article)
  end

  def index
    @articles = Article.published
    index_page_seo_tags
  end

  def show_page_seo_tags(article)
    set_meta_tags title: article.seo_title
    set_meta_tags description: article.seo_description
    set_meta_tags keywords: article.seo_keywords
  end

  def index_page_seo_tags
    set_meta_tags description: 'Читать все новости про удаленную работу на Twikle.'
    set_meta_tags keywords: 'новости про удаленную работу, информация про' \
      ' удаленую работу, новости об удаленной работе'
  end
end
