class JobMailer < ActionMailer::Base
  default from: 'confirm@twikle.ru'

  def registration_confirmation(job)
    @job = job
    mail(to: job.email.to_s, subject: 'Подтверждение вакансии на Twikle')
  end
end
