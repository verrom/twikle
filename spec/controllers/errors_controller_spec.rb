require 'rails_helper'

RSpec.describe ErrorsController, type: :controller do
  describe 'GET #not_found' do
    it 'returns 404 status' do
      get :not_found
      expect(response).to have_http_status(404)
    end
  end

  describe 'GET #internal_server_error' do
    it 'returns 500 status' do
      get :internal_server_error
      expect(response).to have_http_status(500)
    end
  end

  describe 'GET #unprocessable_entity' do
    it 'returns 422 status' do
      get :unprocessable_entity
      expect(response).to have_http_status(422)
    end
  end
end
