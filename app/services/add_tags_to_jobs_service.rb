class AddTagsToJobsService
  TAGS_TO_ADD = ActsAsTaggableOn::Tagging
                .includes(:tag).where(context: 'tech_skills')
                .pluck(:name).compact

  TAGS_TO_ADD_DOWNCASED = TAGS_TO_ADD.map(&:downcase).uniq
  JOBS_TO_TAG = Job.where(created_at: 1.day.ago..Time.now)

  def add_tags_to_jobs
    JOBS_TO_TAG.each do |job|
      job_tags = job.tags.pluck(:name)
      job_tags_downcased = job_tags.map(&:downcase)

      found_tags = TAGS_TO_ADD_DOWNCASED & job_words_downcased(job)
      found_tags.uniq!

      add_tags_to_job(job, job_tags_downcased, found_tags)
    end
  end

  private

  def job_words_downcased(job)
    [job.title, job.description].join(' ').downcase.split(/\b/)
  end

  def add_tags_to_job(job, job_tags_downcased, found_tags)
    found_tags.each do |tag|
      unless job_tags_downcased.include? tag
        job.tags << ActsAsTaggableOn::Tag.where('lower(name) = ?', tag)
      end
    end
  end
end
