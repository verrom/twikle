# == Schema Information
#
# Table name: articles
#
#  id              :integer          not null, primary key
#  title           :string
#  content         :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  slug            :string
#  seo_title       :string
#  seo_keywords    :string
#  seo_description :string
#  approved        :boolean          default(FALSE)
#  promo_text      :string
#

FactoryGirl.define do
  factory :article do
    
  end
end
