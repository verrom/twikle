ActiveAdmin.register Subscription do
  permit_params :email, { job_category_ids: [] }, :salary, :frequency,
                :news_receiver, programming_list: []

  filter :created_at
  filter :updated_at
  filter :news_receiver
  filter :frequency, as: :select, collection: { 'Каждый день': 1, 'Раз в неделю': 7 }
  filter :job_categories, collection: proc { JobCategory.order(:title) }
  filter :salary
  filter :news_receiver


  index do
    selectable_column
    id_column
    column :email
    column :created_at
    column :email_confirmed
    actions
  end

  filter :email

  form do |f|
    f.semantic_errors(*f.object.errors.keys)
    f.inputs 'Детали текстового блока' do
      f.input :email
      f.input :job_categories, as: :check_boxes

      f.inputs 'Теги' do
      f.collection_select :programming_list,
                          ActsAsTaggableOn::Tag.joins(:taggings)
                            .where('taggings.context' => 'programmings').uniq,
                          :name, :name, {hide_label: true},
                          html_options = {
                            class: 'js-example-basic-multiple',
                            multiple: 'multiple'
                          }
      end

      f.input :created_at, input_html: { disabled: true }
      f.input :email_confirmed, input_html: { disabled: true }
      f.input :salary
      f.input :frequency
      f.input :news_receiver
    end
    f.actions
  end
end
