class AddDescriptionToJobCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :job_categories, :description, :text
    add_column :job_categories, :description_visible, :boolean, default: false
  end
end
