source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

ruby '2.4.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
# Use pg as the database for Active Record
gem 'pg'
# Use Puma as the app server
gem 'puma'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'

gem 'bootstrap-sass'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Figaro for create env vars for secrets
gem 'figaro'

# Forms made easy for Rails
gem 'simple_form'

gem 'slim'

gem 'bootstrap_form'

# Rails wrapper for https://github.com/basecamp/trix
# "A rich text editor for everyday writing"
gem 'trix'

# Ckeditor integration gem for rails
gem 'ckeditor'

gem 'paperclip'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Annotate ActiveRecord models
gem 'annotate'

gem 'activeadmin'
gem 'devise'

# Add support for Russian language
gem 'russian'

# Adds pagination
gem 'kaminari'

gem 'bootstrap-kaminari-views'

gem 'friendly_id'

# Cron jobs in Ruby
gem 'whenever', require: false

# Full text search
gem 'pg_search'

# Search Engine Optimization (SEO) for Ruby on Rails applications
gem 'meta-tags'

# SitemapGenerator is the easiest way to generate Sitemaps in Ruby
gem 'sitemap_generator'

# Ruby library for truncating HTML strings keeping the markup valid
gem 'truncato'

# For ActiveJob support
gem 'sidekiq'

# Sidekiq comes with a Sinatra application
# that can display the current state of a Sidekiq installation.
gem 'sinatra', require: nil

# Track changes to your models' data. Good for auditing or versioning.
gem 'paper_trail'

gem 'acts-as-taggable-on'

# Integrate Select2 javascript library with Rails asset pipeline
gem 'select2-rails'

gem 'httparty'

# Extends ActiveAdmin to enable a set of great optional UX improving add-ons
gem 'activeadmin_addons'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop
  # execution and get a debugger console
  gem 'byebug', platform: :mri

  # testing framework for Rails
  gem 'rspec-rails'
  # Collection of testing matchers extracted from Shoulda
  gem 'shoulda-matchers'
  # command line tool to easily handle events on file system modifications
  gem 'guard'
  # allows to automatically & intelligently launch specs when files are modified
  gem 'guard-rspec', require: false

  # ..is a fixtures replacement with a straightforward definition syntax
  gem 'factory_girl_rails'

  # A web server agnostic rack middleware for defining and applying rewrite rules.
  gem 'rack-rewrite'
end

group :development do
  # automatically reload your browser when 'view' files are modified
  gem 'guard-livereload', require: false
  # help to kill N+1 queries and unused eager loading
  gem 'bullet'
  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'capistrano',         require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
  gem 'capistrano-sidekiq', require: false, git: 'https://github.com/seuros/capistrano-sidekiq.git'
  gem 'capistrano-faster-assets', '~> 1.0'
end
