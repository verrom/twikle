class AddDescriptionTitleToJobCategory < ActiveRecord::Migration[5.1]
  def change
    add_column :job_categories, :description_title, :string
  end
end
