class AddEmploymentToJobs < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :employment, :string
  end
end
