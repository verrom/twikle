class GetDataFromMoikrugRssService
  require 'rss'
  require 'httparty'
  require 'open-uri'
  include ActionView::Helpers::UrlHelper

  URL = 'https://moikrug.ru/vacancies/remote_jobs'
  DOC = Nokogiri::XML(open(URL))

  JOB_CATEGORIES = [
    { 'mk_cat': 'Бэкенд',            'tw_cat': 'Программирование'                  },
    { 'mk_cat': 'Фронтенд',          'tw_cat': 'Программирование'                  },
    { 'mk_cat': 'Приложения',        'tw_cat': 'Программирование'                  },
    { 'mk_cat': 'Дизайн',            'tw_cat': 'Дизайн'                            },
    { 'mk_cat': 'Контент',           'tw_cat': 'Копирайтинг'                       },
    { 'mk_cat': 'Фронтенд',          'tw_cat': 'Программирование'                  },
    { 'mk_cat': 'Тестирование',      'tw_cat': 'Тестирование'                      },
    { 'mk_cat': 'Телеком',           'tw_cat': 'Программирование'                  },
    { 'mk_cat': 'Разработка ПО',     'tw_cat': 'Программирование'                  },
    { 'mk_cat': 'Менеджмент',        'tw_cat': 'Менеджмент & управление проектами' },
    { 'mk_cat': 'Маркетинг',         'tw_cat': 'Маркетинг & smm продвижение'       },
    { 'mk_cat': 'Продажи',           'tw_cat': 'Продажи'                           },
    { 'mk_cat': 'Аналитика',         'tw_cat': 'Аналитика'                         },
    { 'mk_cat': 'Другое',            'tw_cat': 'Другие сферы'                      },
    { 'mk_cat': 'Администрирование', 'tw_cat': 'Системное администрирование'       }
  ]

  def fetch_new_jobs
    fetch_mk_actual_jobs_ids && fetch_mk_jobs_on_twikle
    new_jobs_ids = @mk_actual_jobs_ids - @twikle_mk_jobs
    DOC.css('vacancies').xpath('vacancy').each do |job|
      next unless new_jobs_ids.include? URI.parse(job.xpath('url').text).path.split('/').last.to_i
      save_each_job(job)
    end
  end

  def archive_expired_jobs
    fetch_mk_actual_jobs_ids && fetch_mk_jobs_on_twikle

    expired_jobs_ids = @twikle_mk_jobs - @mk_actual_jobs_ids
    expired_jobs_ids.each do |exp_job_id|
      Job.where(provider: 'moikrug', external_id: exp_job_id).update(expired: true)
    end
  end

  private

  def save_each_job(job)
    new_job = Job.new(
      title: job.xpath('job-name').text,
      description: job.xpath('duty').text,
      how_to_apply: "Откликнуться на данную вакансию можно на сайте #{link_to 'Мой Круг', job.xpath('url').text}",
      company_title: job.xpath('company').xpath('name').text,
      company_url: job.xpath('company').xpath('site').text,
      email: 'moikrug@',
      provider: 'moikrug',
      external_id: receive_external_id(job),
      approved: true,
      unique: true,
      currency: job.xpath('currency').text,
      salary_min: salary_boundary(job, 'от'),
      salary_max: salary_boundary(job, 'до'),
      employment: receive_employment(job),
      job_category_id: reseive_job_category_id(job),
      office_location: job.xpath('addresses').xpath('address').xpath('location').text
    )
    if new_job.save
      save_tags_for_job(job, new_job)
    end
  end

  def salary_boundary(job, boundary)
    splitted_salary = job.xpath('salary').text.downcase.split(/[\s\:]/)

    return unless splitted_salary.include? boundary
    salary_part_by_index(splitted_salary, boundary, 1)

    return unless salary_part_by_index(splitted_salary, boundary, 2) == '000'
    salary_part_by_index(splitted_salary, boundary, 1) + '000'
  end

  def salary_part_by_index(splitted_salary, boundary, index)
    splitted_salary[splitted_salary.find_index(boundary) + index]
  end

  def reseive_job_category_id(job)
    cat = job.xpath('category').xpath('specialization').first.text
    tw_cat_title = JOB_CATEGORIES.find { |h| h[:mk_cat] == cat }[:tw_cat]
    JobCategory.find_by(title: tw_cat_title).id
  end

  def receive_external_id(job)
    URI.parse(job.xpath('url').text).path.split('/').last
  end

  def receive_employment(job)
    if job.xpath('employment').text.casecmp('полная').zero?
      'full'
    elsif job.xpath('employment').text.casecmp('частичная').zero?
      'part'
    end
  end

  def save_tags_for_job(job_data, saved_job)
    exist_tags = Tag.pluck(:name).uniq # update after saving each job
    exist_tags_downcased = exist_tags.map(&:downcase).uniq
    job_tags = []
    unless job_data.xpath('requirement').xpath('qualification').empty?
      job_data.xpath('requirement').xpath('qualification').text.split(",").each do |tag|
        job_tags << tag.strip
      end
      job_tags_downcased = job_tags.map(&:downcase)
      intersections = []
      intersections = job_tags_downcased & exist_tags_downcased
      if intersections.any?
        intersections.each do |intersec|
          saved_job.tags << ActsAsTaggableOn::Tag.where('lower(name) = ?', intersec).first
          job_tags.delete_if { |t| t.casecmp(intersec) == 0 }
        end
      end
      job_tags.map { |tag| saved_job.tags.create(name: tag) }
    end
  end

  def fetch_mk_actual_jobs_ids
    @mk_actual_jobs_ids = []
    DOC.css('vacancies').xpath('vacancy').each do |job|
      @mk_actual_jobs_ids << receive_external_id(job).to_i
    end
  end

  def fetch_mk_jobs_on_twikle
    @twikle_mk_jobs = []
    Job.where(provider: 'moikrug').map { |job| @twikle_mk_jobs << job.external_id }
  end
end
