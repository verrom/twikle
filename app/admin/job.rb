ActiveAdmin.register Job do
  permit_params :title, :category, :office_location, :description,
                :how_to_apply, :company_title, :company_url, :email,
                :approved, :email_confirmed, :unique, :salary_min, :salary_max,
                :currency, :job_category_id, :promo_text, :employment,
                :expired, tag_ids: []

  filter :title
  filter :job_category_id,
         collection: proc { JobCategory.order(:title) }, as: :select
  filter :description
  filter :company_title
  filter :email
  filter :approved
  filter :email_confirmed

  index do
    selectable_column
    column :title
    column :job_category_id do |job|
      job.job_category.title unless job.job_category.blank?
    end

    column :company_title
    column :approved
    column :email_confirmed
    actions
  end

  form do |f|
    f.semantic_errors(*f.object.errors.keys)
    f.inputs do
      f.input :title, label: 'Название вакансии'

      f.input :job_category_id,
              as: :select,
              collection: JobCategory.all,
              label: 'Категория'
      f.input :employment,
              as: :select,
              collection: {
                'Полная': 'full',
                'Частичная': 'part',
                'Проектная работа': 'project',
                'Стажировка': 'probation',
                'Волонтерство': 'volunteer'},
              label: 'Тип занятости'

      f.inputs 'Зарплата' do
        f.input :salary_min, label: 'Зарплата от'
        f.input :salary_max, label: 'Зарплата до'
        f.input :currency,
                as: :select,
                collection: { 'Руб.': 'RUR', 'USD': 'USD', 'EUR': 'EUR' },
                label: 'Валюта'
      end

    end
    f.inputs 'Описание вакансии' do
      f.trix_editor :description
    end

    f.inputs 'Теги' do
      f.input :tags, label: 'Теги',
                     input_html: { class: 'js-example-basic-multiple', multiple: 'multiple' }
    end

    f.inputs 'Как откликнуться' do
      f.trix_editor :how_to_apply
    end

    f.inputs 'О компании' do
      f.input :office_location, label: 'Офис'
      f.input :company_title, label: 'Название компании'
      f.input :company_url, label: 'Сайт компании'
      f.input :email
    end

    f.inputs 'Промо текст' do
      f.input :promo_text
    end

    f.inputs 'Модерация' do
      f.input :approved, as: :boolean, label: 'Модерация пройдена'
      f.input :email_confirmed, input_html: {disabled: true }, as: :boolean, label: 'Email подтвержден'
      f.input :expired, label: 'Вакансия в архиве'
    end

    f.inputs 'SEO' do
      f.input :unique, as: :boolean, label: 'Индексировать вакансию'
    end

    f.actions
  end

  # find record with slug(friendly_id)
  controller do
    def find_resource
      begin
        scoped_collection.where(slug: params[:id]).first!
      rescue ActiveRecord::RecordNotFound
        scoped_collection.find(params[:id])
      end
    end
  end
end
