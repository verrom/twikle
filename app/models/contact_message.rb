# == Schema Information
#
# Table name: contact_messages
#
#  id         :integer          not null, primary key
#  name       :string
#  message    :text
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ContactMessage < ApplicationRecord
  validates :name, :message, :email, presence: true
end
