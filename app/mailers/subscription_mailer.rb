class SubscriptionMailer < ActionMailer::Base
  default from: 'no-reply@twikle.ru'

  def subscription_confirmation(subscription)
    @subscription = subscription
    mail(to: subscription.email.to_s,
         subject: 'Подтверждение подписки на Twikle')
  end

  def send_new_jobs_every_day(subscriber, jobs_to_send)
    @jobs_to_send = jobs_to_send
    @subscriber = subscriber
    mail(to: subscriber.email.to_s,
         subject: 'Новые вакансии за последние сутки на Twikle')
  end

  def send_new_jobs_every_week(subscriber, jobs_to_send)
    @jobs_to_send = jobs_to_send
    @subscriber = subscriber
    mail(to: subscriber.email.to_s,
         subject: 'Новые вакансии за последнюю неделю на Twikle')
  end

  def send_new_articles_every_week(subscriber, articles_to_send)
    @articles_to_send = articles_to_send
    @subscriber = subscriber
    mail(to: subscriber.email.to_s,
         subject: 'Новости за последнюю неделю на Twikle')
  end
end
