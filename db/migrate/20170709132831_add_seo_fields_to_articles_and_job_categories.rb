class AddSeoFieldsToArticlesAndJobCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :job_categories, :seo_title, :string
    add_column :job_categories, :seo_keywords, :string
    add_column :job_categories, :seo_description, :string
    add_column :articles, :seo_title, :string
    add_column :articles, :seo_keywords, :string
    add_column :articles, :seo_description, :string
  end
end
