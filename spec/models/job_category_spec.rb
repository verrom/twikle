# == Schema Information
#
# Table name: job_categories
#
#  id                  :integer          not null, primary key
#  title               :string
#  priority            :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  slug                :string
#  link_text           :string
#  description         :text
#  description_visible :boolean          default(FALSE)
#  description_title   :string
#  seo_title           :string
#  seo_keywords        :string
#  seo_description     :string
#  search_phrase       :string
#  hh_specialization   :string
#  short_title         :string
#

require 'rails_helper'

RSpec.describe JobCategory, type: :model do
  context 'validations' do
    it { should validate_presence_of :title    }
    it { should validate_presence_of :priority }
  end

  context 'associations' do
    it { should have_many(:jobs).dependent(:nullify) }
  end

  describe 'slug generation' do
    let(:job_category) { create :job_category }

    it 'has proper slug after save' do
      expect(job_category.slug).to eq 'programming'
    end
  end
end
