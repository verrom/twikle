class SetDefaultValueToProviderField < ActiveRecord::Migration[5.1]
  def change
    change_column :jobs, :expired, :boolean, default: false
  end
end
