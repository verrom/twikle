class AddSalaryToJob < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :salary_min, :integer
    add_column :jobs, :salary_max, :integer
    add_column :jobs, :currency, :string
  end
end
