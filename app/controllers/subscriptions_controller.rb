class SubscriptionsController < ApplicationController
  before_action :force_trailing_slash

  def new
    @subscription = Subscription.new
  end

  def create
    @subscription = Subscription.new(subscription_params)
    if @subscription.save
      SubscriptionMailer.subscription_confirmation(@subscription).deliver_later
      show_success_notice
      render :created
    else
      render :new
    end
  end

  def confirm_email
    subscription = Subscription.find_by_confirm_token(params[:id])
    if subscription
      subscription.email_activate
      flash[:success] = 'Email успешно подтвержден! ' \
                        'Мы рады, что Вы с нами!'
    else
      flash[:error] = 'Извините. Что-то пошло не так. Пожалуйста, оформите ' \
      'подписку заново.'
    end
    redirect_to root_url
  end

  def unsubscribe
    subscription = Subscription.find_by_unsubscribe_hash(params[:id])
    subscription.destroy
    flash[:success] = 'Вы успешно отписаны от рассылки.'
    redirect_to root_url
  end

  private

  def subscription_params
    params.require(:subscription).permit(:email, { job_category_ids: [] },
                                         :salary, :frequency, :news_receiver,
                                         programming_list: [])
  end

  def show_success_notice
    flash[:success] = 'Вы успешно подписаны на рассылку! Вам необходимо ' \
                        'подтвердить адрес электронной почты. Пожалуйста, ' \
                        'перейдите по ссылке в письме, которое мы ' \
                        'Вам отправили.'
  end
end
