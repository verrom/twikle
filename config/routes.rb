Rails.application.routes.draw do
  # match '/404', to: 'errors#not_found', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all
  match '/422', to: 'errors#unprocessable_entity', via: :all

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'pages#home'

  resources :jobs
  resources :job_categories
  resources :contact_messages
  resources :articles
  resources :subscriptions

  get 'search', to: 'search#index', as: 'search'
  get 'sitemap', to: 'pages#sitemap'

  require 'sidekiq/web'
  authenticate :admin_user do
    mount Sidekiq::Web => 'sidekiq'
  end

  resources :jobs do
    member do
      get :confirm_email
    end
  end

  resources :subscriptions do
    member do
      get :confirm_email
      get :unsubscribe
    end
  end

  mount Ckeditor::Engine => '/ckeditor'
  match '*path', to: redirect('/'), via: :all
end
