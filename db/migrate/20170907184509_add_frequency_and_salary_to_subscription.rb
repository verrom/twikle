class AddFrequencyAndSalaryToSubscription < ActiveRecord::Migration[5.1]
  def change
    add_column :subscriptions, :frequency, :integer
    add_column :subscriptions, :salary, :integer
  end
end
