class CreateContactMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_messages do |t|
      t.string :name
      t.text :message
      t.string :email

      t.timestamps
    end
  end
end
