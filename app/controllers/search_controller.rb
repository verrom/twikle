class SearchController < ApplicationController
  def index
    @search_results = Job.search_everywhere(params[:query]).published.not_expired
                         .filter(params.slice(:salary_min, :by_category,
                                              :by_tags, :by_currency,
                                              :by_employment))
                         .sorted_with_reorder
                         .join_tags.page(params[:page]).per(15)
    @all_published_jobs = all_published_jobs
    @job_categories = JobCategory.sorted
  end
end
