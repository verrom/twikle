class SetDefaultValueToProviderField2 < ActiveRecord::Migration[5.1]
  def change
    change_column_default(:jobs, :provider, 'user')
  end
end
