class PagesController < ApplicationController
  def home
    all_published_jobs
    @job_categories = JobCategory.sorted
    @text_block = TextBlock.homepage_text
    home_page_seo_tags
  end

  def sitemap
    respond_to do |format|
      format.xml { render file: 'public/shared/sitemap.xml' }
      format.html { redirect_to root_url }
    end
  end

  private

  def home_page_seo_tags
    set_meta_tags description: 'Twikle (twikle.ru) помогает найти удаленную' \
      ' работу. Twikle - это качественная база вакансий для людей, которые' \
      ' хотят работать из дома или любой другой точки планеты.'
    set_meta_tags keywords: 'удаленная работа, удаленные вакансии, работа, поиск' \
      ' удаленных вакансий, работа из дома, работы, удаленную работу, удаленных' \
      ' работ, ищу удаленную работу, работа на дому, дистанционная работа, вакансии' \
      ' онлайн работы, вакансии удаленной работы'
  end
end
