class JobsController < ApplicationController
  before_action :force_trailing_slash

  def new
    @job = Job.new
  end

  def create
    @job = Job.new(job_params)
    if @job.save
      JobMailer.registration_confirmation(@job).deliver_later
      show_success_notice
      render :created
    else
      render :new
    end
  end

  def show
    @job = Job.published.friendly.find(params[:id])
    @next_job = @job.next_job
    @previous_job = @job.previous_job

    show_page_seo_tags(@job)

    if request.path != job_path(@job)
      redirect_to @job, status: :moved_permanently
    end
  end

  def index
    params[:tag] ? jobs_by_tag : all_published_jobs
    @job_categories = JobCategory.sorted
    index_page_seo_tags(params[:tag])
  end

  def confirm_email
    job = Job.find_by_confirm_token(params[:id])
    if job
      job.email_activate
      flash[:success] = 'Вакансия успешно подтверждена! ' \
                        'В ближайшее время она пройдет модерацию и станет ' \
                        'доступна на нашем сайте.'
    else
      flash[:error] = 'Извините. Вакансия не найдена.'
    end
    redirect_to root_url
  end

  private

  def job_params
    params.require(:job).permit(:title, :category, :office_location,
                                :description, :how_to_apply, :company_title,
                                :company_url, :email, :salary_min, :salary_max,
                                :currency, :job_category_id, :employment,
                                tag_list: [])
  end

  def show_success_notice
    flash[:success] = 'Вакансия успешно создана! Вам необходимо ' \
                        'подтвердить адрес электронной почты. Пожалуйста, ' \
                        'перейдите по ссылке в письме, которое мы ' \
                        'Вам отправили.'
  end

  def jobs_by_tag
    @jobs = Job.published.not_expired.tagged_with(params[:tag]).join_tags
               .filter(params.slice(:salary_min, :salary_max, :by_category,
                                    :by_currency, :by_tags, :by_employment))
               .sorted.page(params[:page]).per(15)
  end

  def show_page_seo_tags(job)
    set_meta_tags noindex: true unless job.unique
    set_meta_tags title: "#{job.title} в #{job.company_title}"
    set_meta_tags description: "#{job.short_description(157)}  ..."
    set_meta_tags keywords: "Удаленная вакансия: #{job.title}," \
      " работа в #{job.company_title}"
  end

  def index_page_seo_tags(tag = nil)
    set_meta_tags description: 'Список всех удаленных вакансий' \
      " с навыком #{tag} на Twikle."
    set_meta_tags keywords: "Удаленная работа с навыком #{tag}," \
      " удаленная работа по #{params[:tag]}, #{params[:tag]} удаленная работа"
    set_meta_tags noindex: true
  end
end
