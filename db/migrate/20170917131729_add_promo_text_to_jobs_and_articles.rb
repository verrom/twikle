class AddPromoTextToJobsAndArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :jobs, :promo_text, :string
    add_column :articles, :promo_text, :string
  end
end
